/*-------------------------------------------------------------------
**
**  Fichero:
**    lab2.c  24/2/2015
**
**    (c) J.M. Mendias
**    Programación de Sistemas y Dispositivos
**    Facultad de Informática. Universidad Complutense de Madrid
**
**  Propósito:
**    Test del laboratorio 2
**
**  Notas de diseño:
**    Presupone que todo el SoC ha sido previamente configurado
**    por el programa residente en ROM que se ejecuta tras reset
**
**-----------------------------------------------------------------*/

#define PCONB		(*(volatile unsigned int *)0x1d20008)
#define PDATB		(*(volatile unsigned int *)0x1d2000c)

#define PCONG		(*(volatile unsigned int *)0x1d20040)
#define PDATG		(*(volatile unsigned int *)0x1d20044)
#define PUPG		(*(volatile unsigned int *)0x1d20048)
	
void main(void) 
{

	PCONB = PCONB & 0xF9FF;	// PB[10] = out, PF[9] = out
	PCONG = PCONG & 0x0FFF;	// PG[7] = in, PG[6] = in
    PUPG  = PUPG | 0xc0;   // Deshabilita pull-up de PG[7] y PG[6]

    // PCONB &= ~((1 << 9) | (1 << 10));
    // PCONG &= ~((0xF << 6));
    // PUPG |= ((1 << 6) | (1 << 7));

	while( 1 )
	    PDATB = PDATG << 3;	// PB[10:9] = PG[7:6]
}
