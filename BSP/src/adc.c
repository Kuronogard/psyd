
/*

** Idenficadores de los canales del conversor

#define ADC_AIN0 (0) 
#define ADC_AIN1 (1)        
#define ADC_AIN2 (2)        
#define ADC_AIN3 (3)        
#define ADC_AIN4 (4)        
#define ADC_AIN5 (5)        
#define ADC_AIN6 (6)        
#define ADC_AIN7 (7)        
*/
#include <s3cev40.h>
#include <s3c44b0x.h>
#include <timers.h>
#include <adc.h>


extern void isr_ADC_dummy( void );

static uint8 state;

/* 
** Inicializa el conversor A/D
*/
void adc_init( void ){
	ADCPSR = 19;
	adc_off();
}

/* 
** Enciende el conversor A/D
*/
void adc_on( void ){
	ADCCON &= ~(1 << 5);
	sw_delay_ms( 10 );
	state = ON;
}

/* 
** Apaga el conversor A/D
*/
void adc_off( void ){
	ADCCON = (1 << 5);
	state = OFF;
}

/*
** Devuelve el estado del conversor A/D
*/
uint8 adc_status( void ){
	return state;
}

/*
** Devuelve la media de 5 lecturas consecutivas del conversor A/D
*/
uint16 adc_getSample( uint8 ch ){
	uint32 sample;
	uint8 i, j;

	ADCCON = (ADCCON & ~(7 << 2)) | (ch << 2);
	sw_delay_ms( 10 );
	for(i = 0 , sample = 0; i < 5 ; i++){
		ADCCON |= 1;
		while( ADCCON & 1 );
		while( !(ADCCON & (1 << 6)) );
		for(j=0;j<ADCPSR;j++);	// To avoid a flag problem that informs the manual
		sample += ADCDAT & 0x3ff;
	}

	return sample/5;
}

/* 
** Instala, en la tabla de vectores de interrupción, la función isr como RTI de interrupciones por fin de conversión A/D
** Borra interrupciones pendientes por fin de conversión A/D
** Desenmascara globalmente las interrupciones y específicamente las interrupciones por fin de conversión A/D
*/
void adc_open( void (*isr)(void) ){
	pISR_ADC = (uint32) isr;
	I_ISPC     = BIT_ADC;
	INTMSK    &= ~(BIT_GLOBAL | BIT_ADC);
}

/* 
** Enmascara las interrupciones por fin de conversión A/D
** Desinstala la RTI por fin de conversión A/D
*/
void adc_close( void ){
	INTMSK |= BIT_ADC;
	pISR_ADC = (uint32)isr_ADC_dummy;
}
