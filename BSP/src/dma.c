#include <s3cev40.h>
#include <s3c44b0x.h>
#include <uart.h>
#include <dma.h>

extern void isr_BDMA0_dummy( void );

/* 
** Inicializa a 0 los registros de control del canal BDMA0
*/
void bdma0_init( void ){
	BDCON0  = 0;
	BDISRC0 = 0;
	BDIDES0 = 0;
	BDICNT0 = 0;
}

/* 
** Instala, en la tabla de vectores de interrupción, la función isr como RTI de interrupciones del canal BDMA0
** Borra interrupciones pendientes por BDMA0
** Desenmascara globalmente las interrupciones y específicamente las interrupciones del canal BDMA0
*/
void bdma0_open( void (*isr)(void) ){
	pISR_BDMA0 = (uint32) isr;
	I_ISPC     = BIT_BDMA0;
	INTMSK    &= ~(BIT_GLOBAL | BIT_BDMA0);
}

/* 
** Deshabilita las interrupciones del canal BDMA0
** Desinstala la RTI del canal BDMA0
*/
void bdma0_close( void ){
	INTMSK |= BIT_BDMA0;
	pISR_BDMA0 = (uint32)isr_BDMA0_dummy;
}
