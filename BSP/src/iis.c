/*-------------------------------------------------------------------


#define IIS_DMA 	(1)
#define IIS_POLLING (2)
*/

#include <s3cev40.h>
#include <s3c44b0x.h>
#include <dma.h>
#include <iis.h>

/*
** Configura el controlador de IIS seg�n los siguientes par�metros 
**   Master mode en reposo (no transfer y todo desabilitado)
**   fs: 16000 KHz
**   CODECLK: 256fs 
**   SDCLK: 32fs
**   Bits por canal: 16
**   Protocolo de trasmisi�n de audio: iis
**   Se�alizacion de canal izquierdo: a baja** Si mode = IIS_DMA
**   No transfer mode
**   Deshabilita Tx/Rx FIFOs
**   Deshabilita prescaler e IIS
**   Inicializa el BDMA0
**   Abre las interrupciones por BDMA0
** Si mode = IIS_POLLING
**   Transmit and receive mode
**   Tx/Rx por pooling
**   Habilita Tx/Rx FIFOs
**   Habilita prescaler e IIS
*/

static void isr_bdma0( void ) __attribute__ ((interrupt ("IRQ")));

static uint8 iomode;
static uint8 flag;

void iis_init( uint8 mode ){
	iomode = mode;

	IISPSR  = 0x77;

	switch(mode){
		case IIS_POLLING:
			IISMOD  = 0xc9;
			IISFCON = 0x300;
			IISCON  = 0x3;
			break;
		case IIS_DMA:
			IISMOD  = 0x9;
			IISFCON = 0xc00;
			IISCON  = 0xc;
			bdma0_init();
			bdma0_open( isr_bdma0 );
			flag = OFF;
			break;
	}
}

/*
** Env�a por el bus IIS una muestra por pooling
*/
inline void iis_putSample( int16 ch0, int16 ch1 ){
	while( ((IISFCON & 0xf0) >> 4) >= 6 );
	IISFIF = ch0;
	IISFIF = ch1;
}

/*
** Almacena por pooling una muestra recibida por el bus IIS
*/
inline void iis_getSample( int16 *ch0, int16 *ch1 ){
	while( (IISFCON & 0xf) < 2);
	*ch0 = IISFIF;
	*ch1 = IISFIF;
}

/*
** Env�a por el bus IIS un flujo de length/2 muestras almacenado en el buffer indicado
** Si mode = IIS_POOLING
**   Transmite las muestras de 1 en 1 ley�ndolas del buffer
** Si mode = IIS_DMA
**   Programa una transferencia por BDMA0 de length bits de buffer a IISFIF
**   Transmit mode
**   Habilita Tx FIFO
**   Tx por DMA
**   Habilita Tx DMA request, prescaler e IIS
** El parametro loop (ON/OFF) permite indicar reproducci�n continua en caso de DMA. No aplica a pooling.
*/
void iis_play( int16 *buffer, uint32 length, uint8 loop ){
	uint32 i;
	int16 ch1, ch2;

	//IISCON &= ~(1 << 3);

	switch(iomode){
		case IIS_POLLING:
			for( i = 0 ; i < length/2 ; ){
				ch1 = buffer[i++];
				ch2 = buffer[i++];
				iis_putSample(ch1, ch2);
			}
			break;
		case IIS_DMA:
			while(flag != OFF);
			flag = ON;

			BDISRC0 = (1 << 30) | (1 << 28) | (uint32) buffer;
			BDIDES0 = (1 << 30) | (3 << 28) | (uint32) &IISFIF;
			BDCON0 = 0;

			if(loop)
				BDICNT0 = (1 << 30) | (1 << 26) | (1 << 22) | (1 << 21) | length;
			else
				BDICNT0 = (1 << 30) | (1 << 26) | (3 << 22) | length;


			BDICNT0 |= (1 << 20);

			// enable Txfifo
			IISFCON = 0xe00;

			// enable DMA request
			IISMOD = 0x89;

			// enable IIS interface
			IISCON = 0x26;
			IISCON |= 1;

			break;
	}
}

/*
** Almacena en el buffer indicado por DMA un flujo de length/2 recibidas por el bus IIS
** utilizando el m�todo de transferencia indicado al inicializar el dispositivo.
** Si mode = IIS_POOLING
**   Recibe las muestras de 1 en 1 almacen�ndolas en el buffer
** Si mode = IIS_DMA
**   Programa una transferencia por BDMA0 de length bits de buffer a IISFIF
**   Reciebe mode
**   Habilita Rx FIFO
**   Rx por DMA
**   Habilita Rx DMA request, prescaler e IIS
*/
void iis_rec( int16 *buffer, uint32 length ){
	uint32 i;
	
	switch(iomode){

		case IIS_POLLING:
			for( i = 0 ; i < length/2 ; i+=2 ){
				iis_getSample(&buffer[i], &buffer[i+1]);
			}
			break;

		case IIS_DMA:
			while(flag != OFF);
			// TODO Si funciona el play, arreglar esto tambien
			flag = ON;

			BDISRC0 = (1 << 30) | (3 << 28) | (uint32) &IISFIF;
			BDIDES0 = (2 << 30) | (1 << 28) | (uint32) buffer;
			BDCON0 = 0;
			BDICNT0 = (1 << 30) | (1 << 26) | (3 << 22 ) | length;

			BDICNT0 |= (1 << 20);

			// enable Rxfifo
			IISFCON = 0xd00;

			// enable DMA request
			IISMOD = 0x49;

			// enable IIS interface
			IISCON = 0x1a;
			IISCON |= 1;
			break;
	}
}

/*
** Pausa la recepcion/transmision de muestras por bus IIS
** Aplica solo al caso de transferencias por DMA
*/
void iis_pause( void ){
	flag = OFF;
	IISCON &= ~1;
}

/*
** Continua la recepcion/transmision de muestras por bus IIS
** Aplica solo al caso de transferencias por DMA
*/
void iis_continue( void ){
	if( flag == ON )
		return;
	flag = ON;
	IISCON |= 1;
}

/*
** Devuelve (ON/OFF) para indicar si se est� reproduciendo o no sonido
** Aplica solo al caso de transferencias por DMA
*/
uint8 iis_status( void ){
	return flag;
}

/*
** Reproduce un fichero en formato WAV cargado en memoria a partir de la direcci�n indicada
*/
void iis_playwawFile( uint8 *fileAddr ){

	uint32 size;

	while( !( fileAddr[0] == 'd' &&
			  fileAddr[1] == 'a' &&
			  fileAddr[2] == 't' &&
			  fileAddr[3] == 'a') ){
		fileAddr++;
	}

	fileAddr += 4;
	size = (uint32) fileAddr[0];
	size += (uint32) fileAddr[1] << 8;
	size += (uint32) fileAddr[2] << 16;
	size += (uint32) fileAddr[3] << 24;
	fileAddr += 4;

	iis_play( (int16 *)fileAddr, size, OFF );
}


static void isr_bdma0( void ){
	IISCON &= ~1;
	flag = OFF;
	I_ISPC = BIT_BDMA0;
}
