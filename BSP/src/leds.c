#include <s3c44b0x.h>
#include <s3cev40.h>
#include <leds.h>

#define LEDS PDATB
static uint8 state;

void leds_init( void ){
	state = 0x3;
	PDATB = (PDATB & ~(0x3 << 9)) | (state << 9);
}

void led_on( uint8 led ){
	state &= ~led;
	PDATB = (PDATB & ~(0x3 << 9)) | (state << 9);
}

void led_off( uint8 led ){
	state |= led;
	PDATB = (PDATB & ~(0x3 << 9)) | (state << 9);
}

void led_toggle( uint8 led ){
	state ^= led;
	PDATB = (PDATB & ~(0x3 << 9)) | (state << 9);
}

uint8 led_status( uint8 led ){
	return state;
}
