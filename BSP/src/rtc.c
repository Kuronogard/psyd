/*-------------------------------------------------------------------
**
**  Fichero:
**    rtc.c  19/4/2013
**
**    D. Pinto
**    Programación de Sistemas y Dispositivos
**    Facultad de Informática. Universidad Complutense de Madrid
**
**  Propósito:
**    Contiene la implementación de las funciones para la gestión 
**    básica del Real Time Clock del chip S3C44BOX
**
**  Notas de diseño:
**
**-----------------------------------------------------------------*/

#include <rtc.h>
#include <s3cev40.h>
#include <s3c44b0x.h>


extern void isr_TICK_dummy( void );

// base 10, bcd <-> binary conversion functions
uint8 bcd_to_binary(uint32 bcdnum){
	return (bcdnum & 0x0f) + (((bcdnum & 0xf0) >> 4)*10);
}

uint32 binary_to_bcd(uint8 binarynum){
	return ((binarynum / 10) << 4) + (binarynum % 10);
}


/*
** Configura el RTC desactivando alarma, round reset y generación de ticks
** Inicializa el RTC a las 00:00:00 horas del martes 1 de enero de 2013
** Inicializa a 0 los registros de alarma 
*/
void rtc_init( void ){
	TICNT  = 0x0;    // 0xxxxxxx 
	RTCALM = 0x0;
	RTCRST = 0x0;
	RTCCON = 0x1;
	BCDYEAR = 0x13;
	BCDMON = 0x01;
	BCDDAY = 0x01;
	BCDDATE = 0x03;
	BCDHOUR = 0x00;
	BCDMIN = 0x00;
	BCDSEC = 0x00;
	ALMYEAR = 0x00;
	ALMMON = 0x00;
	ALMDAY = 0x00;
	ALMHOUR = 0x00;
	ALMMIN = 0x00;
	ALMSEC = 0x00;
	RTCCON &= 0xe;
}

/*
** Actualiza la fecha y hora mantenida por el RTC 
*/
void rtc_puttime( rtc_time_t *rtc_time ){
	RTCCON |= 0x1;

	BCDYEAR = binary_to_bcd(rtc_time->year);
	BCDMON = binary_to_bcd(rtc_time->mon);
	BCDDAY = binary_to_bcd(rtc_time->mday);
	BCDDATE = binary_to_bcd(rtc_time->wday);
	BCDHOUR = binary_to_bcd(rtc_time->hour);
	BCDMIN = binary_to_bcd(rtc_time->min);
	BCDSEC = binary_to_bcd(rtc_time->sec);

	RTCCON &= 0xe;
}

/* 
** Recupera en la fecha y hora mantenida por el RTC
*/
void rtc_gettime( rtc_time_t *rtc_time ){
	RTCCON |= 0x1;
	rtc_time->year = bcd_to_binary(BCDYEAR);
	rtc_time->mon = bcd_to_binary(BCDMON);
	rtc_time->mday = bcd_to_binary(BCDDAY);
	rtc_time->wday = bcd_to_binary(BCDDATE);
	rtc_time->hour = bcd_to_binary(BCDHOUR);
	rtc_time->min = bcd_to_binary(BCDMIN);
	rtc_time->sec = bcd_to_binary(BCDSEC);
	if( ! rtc_time->sec ){
		rtc_time->year = bcd_to_binary(BCDYEAR);
		rtc_time->mon = bcd_to_binary(BCDMON);
		rtc_time->mday = bcd_to_binary(BCDDAY);
		rtc_time->wday = bcd_to_binary(BCDDATE);
		rtc_time->hour = bcd_to_binary(BCDHOUR);
		rtc_time->min = bcd_to_binary(BCDMIN);
		rtc_time->sec = bcd_to_binary(BCDSEC);
	}
	RTCCON &= 0xe;
}

/* 
** Instala, en la tabla de vectores de interrupción, la función isr como RTI de interrupciones por ticks del RTC
** Desenmascara globalmente las interrupciones y específicamente las interrupciones por ticks del RTC
** Habilita la generación de ticks y fija el valor inicial del contador que los genera: perido = (tick_count+1)/128 segundos
*/
void rtc_open( void (*isr)(void), uint8 tick_count ){
	pISR_TICK = (uint32) isr;
	I_ISPC = ~0;
	INTMSK &= 0x3efffff;
	TICNT = 0x80 | tick_count;
}

/*
** Deshabilita la generación de ticks
** Enmascara las interrupciones por ticks del RTC
** Desinstala la RTI por ticks del RTC
*/
void rtc_close( void ){
	TICNT = 0x00;
	INTMSK |= 0x0100000;
	pISR_TICK = (uint32) isr_TICK_dummy;
}

