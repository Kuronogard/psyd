
#include <timers.h>
#include <s3cev40.h>
#include <s3c44b0x.h>
/*
** Modo de funcionamiento del temporizador

** #define TIMER_ONE_SHOT (0)
** #define TIMER_INTERVAL (1)
*/


static uint32 loop_ms = 0;
static uint32 loop_s = 0;

static void sw_delay_init( void );


void timers_init( void ){
	TCFG0 = 0;
	TCFG1 = 0;
	TCNTB0 = 0;
	TCNTB1 = 0;
	TCNTB2 = 0;
	TCNTB3 = 0;
	TCNTB4 = 0;
	TCNTB5 = 0;

	TCMPB0 = 0;
	TCMPB1 = 0;
	TCMPB2 = 0;
	TCMPB3 = 0;
	TCMPB4 = 0;

	TCON = 0x444442;  //  0100 0100 0100 0100 0100 0010
	TCON = 0;

	sw_delay_init();
}

static void sw_delay_init( void ){
	uint32 i;
	timer3_start();
	for(i = 1000000; i; i--);
	loop_s = ((uint64)1000000*10000) / timer3_stop();
	loop_ms = loop_s / 1000;
}

void timer3_delay_ms( uint16 n ){
	for( ; n ; n--){
		timer3_start_timeout(10);
		while(!timer3_timeout());
	}
}

void sw_delay_ms( uint16 n ){
	uint32 i;
	for(i = loop_ms*n; i ; i--);
}

void timer3_delay_s( uint16 n ){
	for( ; n ; n--){
		timer3_start_timeout(1000);
		while(!timer3_timeout());
	}
}

void sw_delay_s( uint16 n ){
	uint32 i;
	for(i = loop_s*n; i ; i--);
}

void timer3_start( void ){
	TCFG0	= (TCFG0 & ~(0xff << 8)) | (199 << 8);
	TCFG1	= (TCFG1 & ~(0xf << 12)) | (0x4 << 12);
	TCNTB3	= 0xffff;
	TCON	= (TCON & ~(0xf << 16)) | (0x2 << 16);
	TCON	= (TCON & ~(0xf << 16)) | (0x1 << 16);
	while( !TCNTO3 );
}

uint16 timer3_stop( void ){
	TCON &= ~(0x1 << 16);
	return 0xffff - TCNTO3;
}

void timer3_start_timeout( uint16 n ){
	TCFG0	= (TCFG0 & ~(0xff << 8)) | (199 << 8);
	TCFG1	= (TCFG1 & ~(0xf << 12)) | (0x4 << 12);
	TCNTB3	= n;
	TCON	= (TCON & ~(0xf << 16)) | (0x2 << 16);
	TCON	= (TCON & ~(0xf << 16)) | (0x1 << 16);

	// Sin el if, si n = 0, bloquearía el sistema para siempre
	if( n != 0)
		while( !TCNTO3 );
}

uint16 timer3_timeout( void ){
	return !TCNTO3;
}

void timer0_open_tick( void (*isr)(void), uint16 tps ){
	pISR_TIMER0 = (uint32) isr;
	I_ISPC 		= BIT_TIMER0;
	INTMSK 		&= ~(BIT_GLOBAL | BIT_TIMER0);

	if( tps > 0 && tps <= 10){
		TCFG0  = (TCFG0 & ~0xff) | (79);
		TCFG1  = (TCFG1 & ~0xf)  | (0x4);  // 1/32
		TCNTB0 = (40000U / tps);
	}else if(tps > 10 && tps <= 100){
		TCFG0  = (TCFG0 & ~0xff) | (4);
		TCFG1  = (TCFG1 & ~0xf)  | (0x4);  // 1/32
		TCNTB0 = (400000U / (uint32)tps);		
	}else if(tps > 100 && tps <= 1000){
		TCFG0  = (TCFG0 & ~0xff) | (0);
		TCFG1  = (TCFG1 & ~0xf)  | (0x3);  // 1/16
		TCNTB0 = (4000000U / (uint32)tps);		
	}else if(tps > 1000){
		TCFG0  = (TCFG0 & ~0xff) | (0);
		TCFG1  = (TCFG1 & ~0xf)  | (0x0);	// 1/2
		TCNTB0 = (3200000U / (uint32)tps);		
	}
	TCON = (TCON & ~0x1f) | (TIMER_INTERVAL << 3) | (0x2);
	TCON = (TCON & ~0x1f) | (TIMER_INTERVAL << 3) | (0x1);
}

void timer0_open_ms( void (*isr)(void), uint16 ms, uint8 mode ){
	pISR_TIMER0 = (uint32) isr;
	I_ISPC 		= BIT_TIMER0;
	INTMSK 		&= ~(BIT_GLOBAL | BIT_TIMER0);
	TCFG0		= (TCFG0 & ~0xff) | (199);
	TCFG1		= (TCFG1 & ~0xf)  | (0x4);
	TCNTB0		= 10 * ms;
	TCON		= (TCON & ~0x1f) | (mode << 3) | (0x2);
	TCON		= (TCON & ~0x1f) | (mode << 3) | (0x1);
}

void timer0_close( void ){
	TCNTB0		= 0;
	TCMPB0		= 0;
	TCON		= (TCON & ~0x1f) | (0x2);
	TCON		= (TCON & ~0x1f);
	INTMSK		|= BIT_TIMER0;
	pISR_TIMER0	= (uint32) NULL;
}
