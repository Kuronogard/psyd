#include <s3c44b0x.h>
#include <s3cev40.h>
#include <uart.h>


/*
 * Compila, pero es un misterio si funciona o no
 * Digo yo que alguna hará lo que tiene que hacer...
 */

void uart0_init( void ){
	ULCON0 = 0x03;
	UMCON0 = 0x00;
	UFCON0 = 0x01;
	UBRDIV0 = 0x22;
	UCON0 = 0x5;
}


void uart0_putchar( char ch ){
	while(UFSTAT0 & 0x0200);
	UTXH0 = ch;
}


void uart0_puts( char *s ){
	while( *s != '\0' ){
		uart0_putchar(*s);
		s++;
	}
}


void uart0_putint( int32 i ){
	char buf[ 11 + 1];
	char *p = buf + 8;
	uint8 neg = 0;

	if( i < 0){
		neg = 1;
		i = -i;
	}

	*p = '\0';
	do{

		*--p = '0' + (i % 10);
		i = i / 10;
	} while( i );

	if(neg)
		*--p = '-';
	
	uart0_puts(p);
}


void uart0_puthex( uint32 i ){
	char buf[ 8 + 1];
	char *p = buf + 8;
	uint8 c;

	*p = '\0';
	do{
		c = i & 0xF;
		if( c < 10 )
			*--p = '0' + c;
		else
			*--p = 'a' + c - 10;
		i = i >> 4;
	} while( i );
	uart0_puts(p);
}


char uart0_getchar( void ){
	while( !(UFSTAT0 & 0x000F) );
	return URXH0;
}


void uart0_gets( char *s ){
	char c = uart0_getchar();
	while( c != '\n' ){
		*s++ = c;
		c = uart0_getchar();
	}
	*s = '\0';
}


int32 uart0_getint( void ){
	int32 result = 0;
	uint8 neg = 0;
	uint8 weight = 1;
	char buff[11 + 1];
	char *p = buff;
	
	char c = uart0_getchar();
	if(c == '-'){
		neg = 1;
		c = uart0_getchar();
	}
	while( c != '\n' ){
		*p++ = c;
		c = uart0_getchar();
	}

	while(p != buff){
		result += (*--p - '0') * weight;
		weight *= 10;
	}

	return (neg)? -result : result;
}


// TODO: Cambiar mujltiplicaciones por desplazamientos
uint32 uart0_gethex( void ){
	uint32 result = 0;
	uint8 weight = 1;
	char buff[8 + 1];
	char *p = buff;
	
	char c = uart0_getchar();
	while( c != '\n' ){
		*p++ = c;
		c = uart0_getchar();
	}

	while(p != buff){
		if( *--p < 'a' )
			result += (*p - '0') * weight;
		else
			result += (*p - 'a' + 10) * weight;

		weight = weight << 4;
	}

	return result;
}
