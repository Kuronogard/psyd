/*-------------------------------------------------------------------

#ifndef __UDA1341TS_H__
#define __UDA1341TS_H__

#include <common_types.h>

#define DAC (1)
#define ADC (2)

#define VOL_MAX (0x3F)
#define VOL_MED	(0x20)
#define VOL_MIN (0x0)

#define MUTE_ON  (1)
#define MUTE_OFF (0)
*/

#include <s3cev40.h>
#include <s3c44b0x.h>
#include <l3.h>
#include <uda1341ts.h>


#define ADDRESS (0x05)
#define DATA0 (0x0)
#define DATA1 (0x1)
#define STATUS (0x2)
#define EA (0x18 << 3)
#define ED (0x7 << 5)


static uint8 uda_volume;
static uint8 uda_status0;
static uint8 uda_status1;

/*
** Inicializa el interfaz L3  
** Resetea el audio codec 
** Configura el audio codec seg�n los siguientes par�metros:
**   CODECLK = 256fs
**   Protocolo de trasmisi�n de audio: iis
**   Volumen de reproducci�n m�ximo
**   Selecciona el canal 1 como entrada
**   Habilita el ADC y DAC con 6 dB de ganancia de entrada
**   Fija el volumen m�ximo
*/
void uda1341ts_init( void ){
	L3_init();
	uda_status0 = (2 << 4);
	uda_status1 = (1 << 7);

	// Resetea el Audio Codec Devuelve
	L3_putByte( (ADDRESS << 2) | STATUS, L3_ADDR_MODE );
	L3_putByte( (1 << 6), L3_DATA_MODE );
	// Quita el reset y fija frecuencia y formato de IIS
	L3_putByte( (2 << 4), L3_DATA_MODE ); 
	L3_putByte( (ADDRESS << 2) | DATA0, L3_ADDR_MODE );
	// Selecciona el canal 1
	L3_putByte( EA | (2), L3_DATA_MODE ) ;
	L3_putByte( ED | 1, L3_DATA_MODE );
	uda1341ts_setvol( VOL_MAX );
	uda1341ts_on( DAC );
	uda1341ts_on( ADC );
}

/*
** Habilita/desabilita el silenciado del audio codec
*/
void uda1341ts_mute( uint8 on ){
	L3_putByte( (ADDRESS) << 2 | DATA0, L3_ADDR_MODE );
	L3_putByte( (1 << 7) | (on << 2)  , L3_DATA_MODE );
}

/*
** Enciende el conversor indicado
*/
void uda1341ts_on( uint8 converter ){
	uda_status1 |= (converter);
	L3_putByte( (ADDRESS) << 2 | STATUS, L3_ADDR_MODE );
	L3_putByte( uda_status1  , L3_DATA_MODE );	
}

/*
** Apaga el conversor indicado
*/
void uda1341ts_off( uint8 converter ){
	uda_status1 &= ~(converter);
	L3_putByte( (ADDRESS) << 2 | STATUS, L3_ADDR_MODE );
	L3_putByte( uda_status1  , L3_DATA_MODE );	
}

/*
** Devuelve el estado del conversor indicado
*/
uint8 uda1341ts_status( uint8 converter ){
	return (uda_status1 & 0x3) >> (converter - 1);
}

/*
** Fija el volumen de reproducci�n
*/
void uda1341ts_setvol( uint8 vol ){
	uda_volume = vol;
	L3_putByte( (ADDRESS << 2) | DATA0 , L3_ADDR_MODE );
	L3_putByte( 0x3f - vol , L3_DATA_MODE );
}

/*
** Devuelve el volumen de reproducci�n
*/
uint8 uda1341ts_getvol( void ){
	return uda_volume;
}
