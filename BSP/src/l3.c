/*

#define L3_ADDR_MODE (0)
#define L3_DATA_MODE (1)
*/

#include <s3cev40.h>
#include <s3c44b0x.h>
#include <leds.h>
#include <l3.h>

#define SHORT_DELAY { int8 j; for( j=0; j<4; j++ ); }

/*
** Inicializa a 1 las lineas L3CLOCK y L3MODE  
*/
void L3_init( void ){
	uint8 rled, lled;
	rled = !led_status( RIGHT_LED );
	lled = !led_status( LEFT_LED );

	PDATB = (rled << 10) | (lled << 9) | (1 << 5) | (1 << 4);
}

/*
** Env�a un byte por el interfaz L3 en el modo (ADDR/DATA) indicado 
*/
void L3_putByte( uint8 byte, uint8 mode ){
	uint8 rled, lled;
	uint8 i;

	rled = !led_status( RIGHT_LED );
	lled = !led_status( LEFT_LED );

	PDATB = (rled << 10) | (lled << 9) | (1 << 5) | (mode << 4);
	SHORT_DELAY;

	for( i = 0; i < 8; i++){
		PDATB = (rled << 10) | (lled << 9) | (mode << 4);
		PDATA = ( (byte & ( 1 << i)) >> i) << 9;
		SHORT_DELAY;
		PDATB = (rled << 10) | (lled << 9) | (1 << 5) | (mode << 4);
		SHORT_DELAY;
	}

	PDATB = (rled << 10) | (lled << 9) | (1 << 5) | (1 << 4);
}
