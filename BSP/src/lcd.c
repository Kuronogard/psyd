#include <s3c44b0x.h>
#include <s3cev40.h>
#include <lcd.h>

/*
	#define LCD_WIDTH   (320)
	#define LCD_HEIGHT  (240)

	#define LCD_BUFFER_SIZE    (LCD_WIDTH*LCD_HEIGHT/2) // en bytes con 2 pixels/byte

	#define BLACK       (0xf)
	#define WHITE       (0x0)
	#define LIGHTGRAY   (0x5)
	#define DARKGRAY    (0xa)
*/

static uint8 lcd_buffer[LCD_BUFFER_SIZE];
extern uint8 font[256*16];

static uint8 status;

/*
** Configura el controlador de LCD según los siguientes parámetros
**   Resolución: 320x240
**   Modo de barrido: 4 bit single scan
**   Modo del lcd: 16 niveles de gris
**   Paletas de color: no
**   Frecuencia de refresco: 60 Hz
**   Reordenado de bytes: activado
**   Self-refresh: desactivado
**   Polaridad de señales de sincronismo: normal
**   Conmutación de VM: en cada frame
**   Anchura del blanking horizontal: mínima
**   Retardo y anchura de pulso de sincronismo horizontal: mínimos
**   Valores de dithering: por defecto
** Inicializa el estado del LCD y lo apaga
*/
void lcd_init( void ){

	DITHMODE = 0x12210;
	DP1_2 = 0xa5a5;
	DP4_7 = 0xba5da65;
	DP3_5 = 0xa5a5f;
	DP2_3 = 0xd6b;
	DP5_7 = 0xeb7b5ed;
	DP3_4 = 0x7dbe;
	DP4_5 = 0x7ebdf;
	DP6_7 = 0x7fdfbfe;
	REDLUT = 0x0;
	GREENLUT = 0x0;
	BLUELUT = 0x0;
	LCDCON1 = 0x1c020;
	LCDCON2 = 0x13cef;
	LCDCON3 = 0x0;
	LCDSADDR1 = (2 << 27) | ((uint32)lcd_buffer >> 1);
	LCDSADDR2 = (1 << 29) | (((uint32)lcd_buffer + LCD_BUFFER_SIZE) & 0x3fffff) >> 1;
	LCDSADDR3 = 0x50;

	lcd_off();
}

/* 
** Enciende el LCD
*/
void lcd_on( void ){
	status = 1;
	LCDCON1 |= 0x1;
}

/* 
** Apaga el LCD
*/
void lcd_off( void ){
	status = 0;
	LCDCON1 &= ~0x1;
}

/*
** Devuelve el estado (LCD_ON/LCD_OFF) del LCD
*/
uint8 lcd_status( void ){
	return status;
}

/*
** Borra el LCD
*/
void lcd_clear( void ){
	uint16 x, y;

	for(x = 0 ; x < LCD_WIDTH ; x++){
		for(y = 0 ; y < LCD_HEIGHT ; y++){
			lcd_putpixel(x,y, WHITE);
		}
	}
}

/*
** Pone el pixel (x,y) en el color indicado
*/
void lcd_putpixel( uint16 x, uint16 y, uint8 color){
	uint8 byte, bit;
	uint16 i;

	i = x/2 + y * (LCD_WIDTH/2);
	bit = (1 - x%2) * 4;

	byte = lcd_buffer[i];
	byte &= ~(0xf << bit);
	byte |= color << bit;
	lcd_buffer[i] = byte;

}

/*
** Devuelve el color al que está el pixel (x,y)
*/
uint8 lcd_getpixel( uint16 x, uint16 y ){
	uint8 byte, bit;
	uint16 i;

	i = x/2 + y * (LCD_WIDTH/2);
	bit = (1 - x%2) * 4;

	byte = lcd_buffer[i];
	byte = (byte >> bit) & 0x0f;

	return byte;
}

/*
** Dibuja una línea horizontal desde el pixel (xleft,y) hasta el pixel (xright,y) del color y grosor indicados
*/
void lcd_draw_hline( uint16 xleft, uint16 xright, uint16 y, uint8 color, uint16 width ){
	uint16 j;
	uint16 x;

	for(x = xleft ; x < xright ; x++){
		for( j = 0 ; j < width ; j++){
			lcd_putpixel(x,y+j,color);
		}
	}
}

/*
** Dibuja una línea vertical desde el pixel (x,yup) hasta el pixel (x,ydown) del color y grosor indicados
*/
void lcd_draw_vline( uint16 yup, uint16 ydown, uint16 x, uint8 color, uint16 width ){
	uint16 y;
	uint16 i;

	for(i = 0 ; i < width ; i++){
		for( y = yup ; y < ydown ; y++){
			lcd_putpixel(x+i,y,color);
		}
	}
}

/*
** Dibuja un rectángulo cuya esquina superior izquierda está en el pixel (xleft,yup) y cuya esquina inferior está en el píxel (xright, ydown) del color y grosor indicados
*/
void lcd_draw_box( uint16 xleft, uint16 yup, uint16 xright, uint16 ydown, uint8 color, uint16 width ){

	lcd_draw_hline(xleft, xright, yup, color, width);
	lcd_draw_hline(xleft, xright, ydown, color, width);
	lcd_draw_vline(yup, ydown, xleft, color, width);
	lcd_draw_vline(yup, ydown, xright, color, width);
}

/*
** Usando una fuente 8x16, escribe un caracter a partir del pixel (x,y) en el color indicado
*/
void lcd_putchar( uint16 x, uint16 y, uint8 color, char ch ){
	uint8 line, row;
	uint8* bitmap;

	bitmap = font + ch*16;
	for( line = 0; line < 16 ; line++){
		for(row = 0 ; row < 8 ; row++){
			if( bitmap[line] & (0x80 >> row) )
				lcd_putpixel(x+row, y+line, color);
			else
				lcd_putpixel(x+row, y+line, WHITE);
		}
	}
}

/*
** Usando una fuente 8x16, escribe una cadena a partir del pixel (x,y) en el color indicado
*/
void lcd_puts( uint16 x, uint16 y, uint8 color, char *s ){
	int i = x;
	int j = y;

	while(*s){
		lcd_putchar(i,j,color,*s);
		s++;
		i += 8;
	}
/*
	while(*s && (i >= 0) && (LCD_WIDTH-i >= 8) && (j >= 0) && (LCD_HEIGHT-j >= 16)   ){
		lcd_putchar(i,j,color,*s);
		s++;
		if(LCD_WIDTH-i < 8){
			i = 0;
			j += 16;
		}
		else{
			i += 8;
		}
	}
*/
}

/*
** Usando una fuente 8x16, escribe una cadena que representa en decimal al entero que toma como argumento a partir del pixel (x,y) en el color indicado
*/
void lcd_putint( uint16 x, uint16 y, uint8 color, int32 i ){
	char buf[ 11 + 1];
	char *p = buf + 8;
	uint8 neg = 0;

	if( i < 0){
		neg = 1;
		i = -i;
	}

	*p = '\0';
	do{

		*--p = '0' + (i % 10);
		i = i / 10;
	} while( i );

	if(neg)
		*--p = '-';
	
	lcd_puts(x,y,color, p);
}

/*
** Usando una fuente 8x16, escribe una cadena que representa en hexadecimal al entero que toma como argumento a partir del pixel (x,y) en el color indicado
*/
void lcd_puthex( uint16 x, uint16 y, uint8 color, uint32 i ){
	char buf[ 8 + 1];
	char *p = buf + 8;
	uint8 c;

	*p = '\0';
	do{
		c = i & 0xF;
		if( c < 10 )
			*--p = '0' + c;
		else
			*--p = 'a' + c - 10;
		i = i >> 4;
	} while( i );

	lcd_puts(x,y,color, p);
}

/*
** Usando una fuente 8x16, escribe un caracter a doble tamaño a partir del pixel (x,y) en el color indicado
*/
void lcd_putchar_x2( uint16 x, uint16 y, uint8 color, char ch ){
	uint8 line, row;
	uint8* bitmap;

	bitmap = font + ch*16;
	for( line = 0; line < 16 ; line++){
		for(row = 0 ; row < 8 ; row++){
			if( bitmap[line] & (0x80 >> row) ){
				lcd_putpixel(x+(row<<1)  , y+(line<<1)  , color);
				lcd_putpixel(x+(row<<1)+1, y+(line<<1)  , color);
				lcd_putpixel(x+(row<<1)  , y+(line<<1)+1, color);
				lcd_putpixel(x+(row<<1)+1, y+(line<<1)+1, color);
			}
			else{
				lcd_putpixel(x+(row<<1)  , y+(line<<1)  , WHITE);
				lcd_putpixel(x+(row<<1)+1, y+(line<<1)  , WHITE);
				lcd_putpixel(x+(row<<1)  , y+(line<<1)+1, WHITE);
				lcd_putpixel(x+(row<<1)+1, y+(line<<1)+1, WHITE);
			}
		}
	}
}

/*
** Usando una fuente 8x16, escribe una cadena a doble tamaño a partir del pixel (x,y) en el color indicado
*/
void lcd_puts_x2( uint16 x, uint16 y, uint8 color, char *s ){
	int i = x;
	int j = y;

	while(*s){
		lcd_putchar_x2(i,j,color,*s);
		s++;
		i += 16;
	}
	/*
	while(s && (i >= 0) && (LCD_WIDTH-i >= 16) && (j >= 0) && (LCD_HEIGHT-j >= 32)   ){
		lcd_putchar_x2(i,j,color,*s);
		s++;
		if(LCD_WIDTH-i < 16){
			i = 0;
			j += 32;
		}
		else{
			i += 16;
		}
	}
	*/
}

/*
** Usando una fuente 8x16, escribe una cadena a doble tamaño que representa en decimal al entero que toma como argumento a partir del pixel (x,y) en el color indicado
*/
void lcd_putint_x2( uint16 x, uint16 y, uint8 color, int32 i ){
	char buf[ 11 + 1];
	char *p = buf + 8;
	uint8 neg = 0;

	if( i < 0){
		neg = 1;
		i = -i;
	}

	*p = '\0';
	do{

		*--p = '0' + (i % 10);
		i = i / 10;
	} while( i );

	if(neg)
		*--p = '-';
	
	lcd_puts_x2(x,y,color, p);
}

/*
** Usando una fuente 8x16, escribe una cadena a doble tamaño que representa en hexadecimal al entero que toma como argumento a partir del pixel (x,y) en el color indicado
*/
void lcd_puthex_x2( uint16 x, uint16 y, uint8 color, uint32 i ){
	char buf[ 8 + 1];
	char *p = buf + 8;
	uint8 c;

	*p = '\0';
	do{
		c = i & 0xF;
		if( c < 10 )
			*--p = '0' + c;
		else
			*--p = 'a' + c - 10;
		i = i >> 4;
	} while( i );

	lcd_puts_x2(x,y,color, p);
}

/*
** Muestra un BMP de 320x240 px y 16b/px
*/
void lcd_putWallpaper( uint8 *bmp ){
	uint32 headerSize;

	uint16 x, ySrc, yDst;
	uint16 offsetSrc, offsetDst;

	headerSize = bmp[10] + (bmp[11]<<8) + (bmp[12]<<16) + (bmp[13]<<24);
	bmp += headerSize;

	for(ySrc = 0 , yDst=240-1 ; ySrc < 240 ; ySrc++, yDst--){
		offsetDst = yDst*320/2;
		offsetSrc = ySrc*320/2;
		for( x = 0 ; x<320/2 ; x++){
			lcd_buffer[offsetDst + x] = ~bmp[offsetSrc + x];
		}
	}
}
