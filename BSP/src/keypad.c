
/* 
** Scancodes del keypad

** #define KEYPAD_KEY0 (0x0)   Distribuci�n espacial:  
#define KEYPAD_KEY1 (0x1)  
#define KEYPAD_KEY2 (0x2)   KEY0  KEY1  KEY2  KEY3  
#define KEYPAD_KEY3 (0x3)   KEY4  KEY5  KEY6  KEY7  
#define KEYPAD_KEY4 (0x4)   KEY8  KEY9  KEYA  KEYB  
#define KEYPAD_KEY5 (0x5)   KEYC  KEYD  KEYE  KEYF  
#define KEYPAD_KEY6 (0x6)
#define KEYPAD_KEY7 (0x7)
#define KEYPAD_KEY8 (0x8)
#define KEYPAD_KEY9 (0x9)
#define KEYPAD_KEYA (0xa)
#define KEYPAD_KEYB (0xb)
#define KEYPAD_KEYC (0xc)
#define KEYPAD_KEYD (0xd)
#define KEYPAD_KEYE (0xe)
#define KEYPAD_KEYF (0xf)


 
** Errorcodes del keypad

#define KEYPAD_FAILURE (0xff)   Fallo durante el proceso de scan 
#define KEYPAD_TIMEOUT (0xfe)   Tiempo de espera sobrepasado 


** Status de una tecla

#define KEY_DOWN   (1)
#define KEY_UP     (0)
**/

#include <keypad.h>
#include <timers.h>
#include <s3cev40.h>
#include <s3c44b0x.h>

extern void isr_KEYPAD_dummy( void );

static void keypad_down_isr( void );
static void keypad_up_isr( void );
static void timer0_down_isr( void );
static void timer0_up_isr( void );

/*
** Inicializa el keypad
** Inicializa timers
*/
void keypad_init( void ){
	//EXTINT = (EXTINT & ~(0xf << 4)) | (2 << 4);
	timers_init();
	//keypad_open( keypad_down_isr );
}

uint8 keypad_scan( void ){
	volatile uint8 aux;

	aux = *( KEYPAD_ADDR + 0x1c );
	if( (aux & 0x1e) != 0x1e ){
		if( (aux & 0x8) == 0)		return KEYPAD_KEY0;
		else if( (aux & 0x4) == 0) 	return KEYPAD_KEY1;
		else if( (aux & 0x2) == 0)	return KEYPAD_KEY2;
		else if( (aux & 0x1) == 0)	return KEYPAD_KEY3;
	}
	
	aux = *( KEYPAD_ADDR + 0x1a );
	if( (aux & 0x1e) != 0x1e ){
		if( (aux & 0x8) == 0)		return KEYPAD_KEY4;
		else if( (aux & 0x4) == 0) 	return KEYPAD_KEY5;
		else if( (aux & 0x2) == 0)	return KEYPAD_KEY6;
		else if( (aux & 0x1) == 0)	return KEYPAD_KEY7;
	}

	aux = *( KEYPAD_ADDR + 0x16 );
	if( (aux & 0x1e) != 0x1e ){
		if( (aux & 0x8) == 0)		return KEYPAD_KEY8;
		else if( (aux & 0x4) == 0) 	return KEYPAD_KEY9;
		else if( (aux & 0x2) == 0)	return KEYPAD_KEYA;
		else if( (aux & 0x1) == 0)	return KEYPAD_KEYB;
	}

	aux = *( KEYPAD_ADDR + 0x0e );
	if( (aux & 0x1e) != 0x1e ){
		if( (aux & 0x8) == 0)		return KEYPAD_KEYC;
		else if( (aux & 0x4) == 0) 	return KEYPAD_KEYD;
		else if( (aux & 0x2) == 0)	return KEYPAD_KEYE;
		else if( (aux & 0x1) == 0)	return KEYPAD_KEYF;
	}

	return KEYPAD_FAILURE;
}

/*
** Devuelve el estado de la tecla indicada
*/
uint8 keypad_status( uint8 scancode ){
	uint8 auxscan, auxscancode;
	auxscan = *( KEYPAD_ADDR + ( 0x1e & ~(0x02 << ((scancode & 0x0c)>>2)) ) );
	auxscancode = (0x8 >> (scancode & 0x3));
	
	if( ((auxscan & 0x1e) != 0x1e) && ((auxscan & auxscancode) == 0) )
		return KEY_DOWN;
	else
		return KEY_UP;

}

/*
** Espera a que se presione la tecla indicada
*/
void keypad_wait_keydown( uint8 scancode ){
	while(1){
		while( PDATG & 0x2 );
		sw_delay_ms( KEYPAD_KEYDOWN_DELAY );
		if( scancode == keypad_scan() )
			return;
		while( !(PDATG & 0x2) );
		sw_delay_ms( KEYPAD_KEYUP_DELAY );
	}
}

/* 
** Espera a que se presione y depresione la tecla indicada
*/
void keypad_wait_keyup( uint8 scancode ){
	uint8 auxscan;
	while(1){
		while( PDATG & 0x2 );
		sw_delay_ms( KEYPAD_KEYDOWN_DELAY );
		auxscan = keypad_scan();
		while( !(PDATG & 0x2) );
		sw_delay_ms( KEYPAD_KEYUP_DELAY );
		if(auxscan == scancode)
			return;
	}
}

/* 
** Espera a que se presione cualquier tecla
*/
void keypad_wait_any_keydown( void ){
	while( PDATG & 0x2 );
	sw_delay_ms( KEYPAD_KEYDOWN_DELAY );
}

/* 
** Espera a que se presione y depresione cualquier tecla
*/
void keypad_wait_any_keyup( void ){
	while( PDATG & 0x2 );
	sw_delay_ms( KEYPAD_KEYDOWN_DELAY );
	while( !(PDATG & 0x2) );
	sw_delay_ms( KEYPAD_KEYUP_DELAY );
}

/* 
** Espera la presi�n y depresi�n de una tecla del keypad y devuelve su scancode
*/ 
uint8 keypad_getchar( void ){
	uint8 scancode;
	while( PDATG & 0x2 );
	sw_delay_ms( KEYPAD_KEYDOWN_DELAY );
	scancode = keypad_scan();
	while( !(PDATG & 0x2) );
	sw_delay_ms( KEYPAD_KEYUP_DELAY );
	return scancode;
}

/* 
** Espera la presi�n y depresi�n de una tecla del keypad y devuelve su scancode y el intervalo en ms que ha estado pulsada (max. 6553ms)
*/ 
uint8 keypad_getchartime( uint16 *ms ){
	uint8 scancode;
	while( PDATG & 0x2 );
	sw_delay_ms( KEYPAD_KEYDOWN_DELAY );
	timer3_start();
	scancode = keypad_scan();
	while( !(PDATG & 0x2) );
	*ms = timer3_stop() / 10;
	sw_delay_ms( KEYPAD_KEYUP_DELAY );
	return scancode;
}

/*
** Espera un m�ximo de ms (no mayor de 6553ms) la presi�n y depresi�n de una tecla del keypad y devuelve su scancode, en caso contrario devuelve KEYPAD_TIMEOUT
*/
uint8 keypad_timeout_getchar( uint16 ms ){
	uint8 scancode;
	timer3_start_timeout(ms * 10);
	while( (PDATG & 0x2) && !timer3_timeout() );
	if(timer3_timeout())
		return KEYPAD_TIMEOUT;
	sw_delay_ms( KEYPAD_KEYDOWN_DELAY );
	scancode = keypad_scan();
	timer3_start_timeout(ms * 10);
	while( !(PDATG & 0x2) && !timer3_timeout() );
	if(timer3_timeout())
		return KEYPAD_TIMEOUT;
	sw_delay_ms( KEYPAD_KEYUP_DELAY );
	return scancode;
}

/* 
** Instala, en la tabla de vectores de interrupci�n, la funci�n isr como RTI de interrupciones por presi�n del keypad
** Borra interrupciones pendientes por presi�n del keypad
** Desenmascara globalmente las interrupciones y espec�ficamente las interrupciones por presi�n del keypad
*/
void keypad_open( void (*isr)(void) ){
	pISR_KEYPAD = (uint32) isr;
	I_ISPC = BIT_EINT1;
	INTMSK &= ~(BIT_GLOBAL | BIT_EINT1);
}

/* 
** Enmascara las interrupciones por presi�n del keypad
** Desinstala la RTI por presi�n del keypad
*/
void keypad_close( void ){
	INTMSK |= BIT_EINT1;
	pISR_KEYPAD = (uint32) isr_KEYPAD_dummy;
}


static void keypad_down_isr( void ){
	timer0_open_ms(timer0_down_isr, KEYPAD_KEYDOWN_DELAY, TIMER_ONE_SHOT);
	INTMSK |= BIT_KEYPAD;
	I_ISPC  = BIT_KEYPAD;
}

static void keypad_up_isr( void ){
	timer0_open_ms(timer0_up_isr, KEYPAD_KEYUP_DELAY, TIMER_ONE_SHOT);
	INTMSK |= BIT_KEYPAD;
	I_ISPC  = BIT_KEYPAD;
}

static void timer0_down_isr( void ){
	// TODO: escanear c�digo y almacenarlo
	// <jgvjv> = keypad_scan();
	EXTINT = (EXTINT & ~(0xf << 4)) | (4 << 4);
	keypad_open( keypad_up_isr );
	I_ISPC = BIT_TIMER0;
}

static void timer0_up_isr( void ){
	EXTINT = (EXTINT & ~(0xf << 4)) | (2 << 4);
	keypad_open( keypad_down_isr );
	I_ISPC = BIT_TIMER0;	
}
