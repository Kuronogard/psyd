
#include <s3cev40.h>
#include <s3c44b0x.h>
#include <timers.h>
#include <dma.h>
#include <iis.h>
#include <iic.h>

/*
** Modos de funcionamiento del controlador de IIC

#define IIC_Rx      (2)
#define IIC_Tx      (3)


** Macros para indicar si en recepci�n se env�a ACK o no

#define RxACK       (1)
#define NO_RxACK    (0)
*/


/*
** Configura el controlador de IIC seg�n los siguientes par�metros 
**   Interrupciones: habilitadas
**   Transmisi�n/recepci�n: activada
**   Frecuencia de comunicaci�n: 250 KHz
**   Generaci�n de ACK: autom�tica
*/
void iic_init( void ){
	IICADD  = 0x0;
	IICCON   = 0xaf;
	IICSTAT = 0x10;
}

/*
** Inicia un flujo de transmici�n/recepci�n en la que el microcontrolador act�a como master:
**   Genenera la START condition
**   Env�a la direcci�n del dispositivo esclavo
**   Espera la recepci�n de ACK 
*/
void iic_start( uint8 mode, uint8 byte ){
	IICDS = byte;
	IICSTAT = (IICSTAT & ~(3 << 6)) | (mode << 6) | (1 << 5) | (1 << 4);
	IICCON &= ~(1 << 4);
	while( !(IICCON & (1<<4)) );
}

/*
** Actuando el microcontrolador como master:
**   Env�a el byte indicado 
**   Espera la recepci�n de ACK
*/
void iic_putByte( uint8 byte ){
	IICDS = byte;
	IICCON &= ~(1 << 4);
	while( !(IICCON & (1<<4)) );
}

/*
** Actuando el microcontrolador como master:
**   Espera la recepci�n de un byte
**   Genera o no ACK 
**   Devuelve el byte recibido
*/
uint8 iic_getByte( uint8 ack ){
	IICCON = (IICCON & ~(1 << 7)) | (ack << 7);
	IICCON &= ~(1 << 4);
	while( !(IICCON & (1 << 4)) );
	return IICDS;
}

/*
** Env�a la stop condition y espera los ms indicados para que haga efecto
*/
void iic_stop( uint16 ms ){
	IICSTAT &= ~(1 << 5);
	IICCON &= ~(1 << 4);
	sw_delay_ms( ms );
}
