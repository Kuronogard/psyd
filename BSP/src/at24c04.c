/*

#define AT24C04_WIDTH (8)
#define AT24C04_DEPTH (512)
*/
#include <iic.h>
#include <at24c04.h>

#define DEVICE_ADDR (( 0xa << 4) | (0<<1) )

#define READ (1)
#define WRITE (0)

/*
** Borra al completo el contenido de la memoria
*/
void at24c04_clear( void ){
	uint16 addr;
	uint8 page;
	uint16 i, j;

	for(i = 0; i < 32; i++){

		page = (addr & 0x100) >> 8;

		iic_start( IIC_Tx, DEVICE_ADDR | (page << 1) | WRITE );
		iic_putByte( addr & 0xff );
		for(j = 0 ; j < 15 ; j++){
			iic_putByte( (uint8)0 );
		}
		iic_stop( 5 );
		addr+=16;
	}
}

/*
** Realiza una escritura aleatoria en la direcci�n indicada
*/
void at24c04_bytewrite( uint16 addr, uint8 data ){
	uint8 page;

	page = (addr & 0x100) >> 8;

	iic_start( IIC_Tx, DEVICE_ADDR | (page << 1) | WRITE );
	iic_putByte( addr & 0xff );
	iic_putByte( data );
	iic_stop( 5 );
}

/*
** Realiza una lectura aleatoria de la direcci�n indicada
*/
void at24c04_byteread( uint16 addr, uint8 *data ){
	uint8 page;

	page = (addr & 0x100) >> 8;

	iic_start( IIC_Tx, DEVICE_ADDR | (page << 1) | WRITE );
	iic_putByte( addr & 0xff );
	iic_start( IIC_Rx, DEVICE_ADDR | (page << 1) | READ );
	*data = iic_getByte( NO_RxACK );
	iic_stop( 5 );
}

/*
** Realiza una lectura secuencial de la memoria completa
*/
void at24c04_load( uint8 *buffer ){
	uint16 i;

	iic_start( IIC_Tx, DEVICE_ADDR | (0 << 1) | WRITE );
	iic_putByte( 0);
	iic_start( IIC_Rx, DEVICE_ADDR | (0 << 1) | READ );

	for(i = 0 ; i < AT24C04_DEPTH-1 ; i++){
		buffer[i] = iic_getByte( RxACK );
	}

	buffer[i] = iic_getByte( NO_RxACK );
	iic_stop( 5 );
}

/*
** Realiza una escritura paginada de la memoria completa
*/
void at24c04_store( uint8 *buffer ){
	uint16 addr = 0;
	uint8 page;
	uint16 i,j;

	for(i = 0; i < 32; i++){

		page = (addr & 0x100) >> 8;

		iic_start( IIC_Tx, DEVICE_ADDR | (page << 1) | WRITE );
		iic_putByte( addr & 0xff );
		for(j = 0 ; j < 15 ; j++){
			iic_putByte( buffer[addr] );
			addr++;
		}
		iic_putByte( buffer[addr] );
		iic_stop( 5 );
		addr++;
	}
}


