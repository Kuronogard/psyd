#include <s3cev40.h>
#include <s3c44b0x.h>
#include <timers.h>
#include <lcd.h>
#include <adc.h>
#include <ts.h>
#include <uart.h>

#define PX_ERROR (5)

#define TS_DOWN_DELAY 200
#define TS_UP_DELAY 100

static uint16 Vxmin = 0;
static uint16 Vxmax = 0;
static uint16 Vymin = 0;
static uint16 Vymax = 0;

static uint8 state;

extern void isr_TOUCHPAD_dummy( void );

static void ts_scan( uint16 *x, uint16 *y);
static void ts_calibrate( void );
static void ts_sample2coord(uint16 Vx, uint16 Vy, uint16 *x, uint16 *y);


/* 
** Inicializa y calibra la touchscreen
*/
void ts_init( void ){
	uart0_init();
	lcd_init();
	adc_init();
	PDATE = (PDATE & ~(0xf << 4)) | (0xb << 4);
	ts_on();
	ts_calibrate();
	ts_off();
}

/* 
** Enciende el ADC que usa la touchscreen
*/
void ts_on( void ){
	adc_on();
	state = TS_ON;
}

/* 
** Apaga el ADC que usa la touchscreen
*/
void ts_off( void ){
	adc_off();
	state = TS_OFF;
}

/*
** Devuelve el estado del ADC que usa la touchscreen
*/
uint8 ts_status( void ){
	return state;
}

/* 
** Espera a que se pulse la touchscreen
*/
void ts_wait_down( void ){
	while( PDATG & (1 << 2) );
	sw_delay_ms(TS_DOWN_DELAY);
}

/* 
** Espera a que se pulse y despulse la touchscreen
*/
void ts_wait_up( void ){
	ts_wait_down();
	while( !(PDATG & (1 << 2)) );
	sw_delay_ms(TS_UP_DELAY);
}

/*
** Espera la pulsaci�n de la touchscreen y devuelve la posicion
*/
void ts_getpos( uint16 *x, uint16 *y ){
	uint16 Vx, Vy;

	ts_wait_down();
	ts_scan(&Vx, &Vy);
	ts_sample2coord( Vx, Vy, x, y );
}

/*
** Espera la pulsaci�n del touchscreen y devuelve la posicion y el intervalo en ms que ha estado pulsada (max. 65535ms)
*/
void ts_getpostime( uint16 *x, uint16 *y, uint16 *ms ){
	uint16 Vx, Vy;

	ts_wait_down();
	timer3_start();

	ts_scan(&Vx, &Vy);
	ts_sample2coord( Vx, Vy, x, y );

	while( !(PDATG & (1 << 2)) );
	*ms = timer3_stop() / 10;
	sw_delay_ms(TS_UP_DELAY);
}

/*
** Espera un m�ximo de ms (no mayor de 6553ms) la pulsaci�n y despulsaci�n de la touchscreen y devuelve la posicion, en caso contrario devuelve TS_TIMEOUT
*/

uint8 ts_timeout_getpos( uint16 *x, uint16 *y, uint16 ms ){
	uint16 Vx, Vy;

	timer3_start_timeout(ms * 10);
	while( (PDATG & (1 << 2)) && !timer3_timeout() );
	if(timer3_timeout())
		return TS_TIMEOUT;
	sw_delay_ms(TS_DOWN_DELAY);

	ts_scan(&Vx, &Vy);
	ts_sample2coord( Vx, Vy, x, y );

	timer3_start_timeout(ms * 10);
	while( !(PDATG & (1 << 2)) && !timer3_timeout() );
	if(timer3_timeout())
		return TS_TIMEOUT;
	sw_delay_ms(TS_UP_DELAY);

	return TS_OK;
}

/* 
** Instala, en la tabla de vectores de interrupci�n, la funci�n isr como RTI de interrupciones por pulsaci�n de la touchscreen
** Borra interrupciones pendientes por pulsaci�n de la touchscreen
** Desenmascara globalmente las interrupciones y espec�ficamente las interrupciones por pulsaci�n de la touchscreen
*/
void ts_open( void (*isr)(void) ){
	pISR_TOUCHPAD = (uint32) isr;
	I_ISPC     = BIT_TOUCHPAD;
	INTMSK    &= ~(BIT_GLOBAL | BIT_TOUCHPAD);
}

/* 
** Enmascara las interrupciones por pulsaci�n de la touchscreen
** Desinstala la RTI por pulsaci�n de la touchscreen
*/
void ts_close( void ){
	INTMSK |= BIT_TOUCHPAD;
	pISR_ADC = (uint32)isr_TOUCHPAD_dummy;
}

static void ts_calibrate( void ){
	uint16 x, y, Vx, Vy;

	uart0_puts("Calibrando el lcd...");
	lcd_clear();
	lcd_on();

	do{
		// pintar un punto en (0,0) {Vxmin, Vymax} y solicita que se presione
		uart0_puts("\nPresione en el punto dibujado en la pantalla\n");
		lcd_putpixel(0,0,BLACK);

		while( PDATG & (1 << 2) );
		sw_delay_ms(TS_DOWN_DELAY);
		ts_scan( &Vxmin, &Vymax);
		while( !(PDATG & (1 << 2)) );
		sw_delay_ms(TS_UP_DELAY);
		lcd_putpixel(0,0,WHITE);
		uart0_puts("Vxmin=");
		uart0_putint(Vxmin);
		uart0_puts("  ");
		uart0_puts("Vymax=");
		uart0_putint(Vymax);


		// pintar punto en (319,239) {Vxmax, Vymin} y solicitar presion
		uart0_puts("\nPresione en el punto dibujado en la pantalla\n");

		lcd_putpixel(319,239,BLACK);

		while( PDATG & (1 << 2) );
		sw_delay_ms(TS_DOWN_DELAY);
		ts_scan( &Vxmax, &Vymin);
		while( !(PDATG & (1 << 2)) );
		sw_delay_ms(TS_UP_DELAY);
		lcd_putpixel(319,239,WHITE);
		uart0_puts("Vxmax=");
		uart0_putint(Vxmax);
		uart0_puts("  ");
		uart0_puts("Vymin=");
		uart0_putint(Vymin);


		// pintar punto en centro (160,120) {centro} y solicitar presion
		uart0_puts("\nPresione en el punto dibujado en la pantalla\n");
		lcd_putpixel(160,120,BLACK);

		while( PDATG & (1 << 2) );
		sw_delay_ms(TS_DOWN_DELAY);
		ts_scan( &Vx, &Vy );
		while( !(PDATG & (1 << 2)) );
		sw_delay_ms(TS_UP_DELAY);
		lcd_putpixel(159,119,WHITE);
		uart0_puts("Vx=");
		uart0_putint(Vx);
		uart0_puts("  ");
		uart0_puts("Vy=");
		uart0_putint(Vy);

		ts_sample2coord(Vx, Vy, &x, &y);
		uart0_puts("x=");
		uart0_putint(x);
		uart0_puts("  ");
		uart0_puts("y=");
		uart0_putint(y);

	} while( (x > 160+PX_ERROR) || (x < 160-PX_ERROR) || 
			 (y > 120+PX_ERROR) || (y < 120-PX_ERROR) );

}

static void ts_scan(uint16 *Vx, uint16 *Vy){
	PDATE = (PDATE & ~(0xf << 4)) | (0x6 << 4);
	*Vx = adc_getSample(ADC_AIN1);

	PDATE = (PDATE & ~(0xf << 4)) | (0x9 << 4);
	*Vy = adc_getSample(ADC_AIN0);

	PDATE = (PDATE & ~(0xf << 4)) | (0xb << 4);
}


static void ts_sample2coord(uint16 Vx, uint16 Vy, uint16 *x, uint16 *y){
	if( Vx < Vxmin )
		*x = 0;
	else if( Vx > Vxmax )
		*x = 319;
	else
		*x = 320*(Vx-Vxmin) / (Vxmax-Vxmin);

	if( Vy < Vymin )
		*y = 240;
	else if( Vy > Vymax )
		*y = 0;
	else
		*y = 240*(Vymax-Vy) / (Vymax-Vymin);
}
