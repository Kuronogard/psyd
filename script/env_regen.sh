#!/bin/bash

#Script to Regenerate PSyD environment

projects="lab1 lab2 lab3 lab4 lab5 lab6 lab7 lab8 lab9-wav lab9-efectos lab10 lab12"

for p in $projects
do
	echo -ne "\tCreating '${p}' project ................. "

	if [ ! -d ../${p} ]; then
		mkdir ../${p}
	fi

	rm -f ../${p}/Makefile
	rm -f ../${p}/.project
	rm -f ../${p}/.cproject
	rm -f ../${p}/${p}.ld

	cat .templates/make.template | sed "s/<project_name>/${p}/g" > ../${p}/Makefile
	cat .templates/eclipseproject.template  | sed "s/<project_name>/${p}/g" > ../${p}/.project
	cat .templates/eclipsecproject.template | sed "s/<project_name>/${p}/g" > ../${p}/.cproject
	cp  .templates/lab.ld.template ../${p}/${p}.ld

	echo "OK"
done
