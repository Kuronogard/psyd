# PSyD project

Project for the subject "PSyD" (Programación de Sistemas y Dispositivos).  

Implementation of a "museum speaker" (I don't know how you, english guys, call that thing) in a naked microcontroller based machine.

Consists in an incremental main project composed of little ones called "labs":

##### Lab1:
>First contact with the environment.

##### Lab2:
>7 segment display implementation.

##### Lab3:
>Simple system initialization (IO ports) and led implementation.

##### Lab4:
>PLL configuration in system init and UART support.
