#=============================================================================================$
#  Daniel Pinto Rivero  - Madrid, 2014
#  Universidad Complutense de Madrid
#
#  PSyD - Ing.Computadores
#=============================================================================================$

#-------------------------------------------------------------
# Files
#-------------------------------------------------------------
LIBS=-lBSP
TARGET=lab4
SOURCES=lab4.c
FLASH=$(TARGET).hex
EXEC=$(TARGET).elf
OBJS=$(SOURCES:.c=.o)


DEBUG_FLASH=$(DEBUG_DIR)/lab4.hex
DEBUG_EXEC=$(DEBUG_DIR)/lab4.elf
DEBUG_OBJS=$(DEBUG_OBJ_DIR)/lab4.o

RELEASE_FLASH=$(RELEASE_DIR)/lab4.hex
RELEASE_EXEC=$(RELEASE_DIR)/lab4.elf
RELEASE_OBJS=$(RELEASE_OBJ_DIR)/lab4.o

#-------------------------------------------------------------
# Directories
#-------------------------------------------------------------
PROJECT_DIR=../lab4
RELEASE_DIR=$(PROJECT_DIR)/Release_
DEBUG_DIR=$(PROJECT_DIR)/Debug_
RELEASE_OBJ_DIR=$(RELEASE_DIR)/obj
DEBUG_OBJ_DIR=$(DEBUG_DIR)/obj
LIB_DIR=../BSP
LIB_DEBUG_DIR=$(LIB_DIR)/Debug_
LIB_RELEASE_DIR=$(LIB_DIR)/Release_
HEAD_DIR=$(LIB_DIR)/include

#-------------------------------------------------------------
# Programs
#-------------------------------------------------------------
CC=arm-none-eabi-gcc
AS=arm-none-eabi-as
AR=arm-none-eabi-ar
LD=arm-none-eabi-ld
RANLIB=arm-none-eabi-ranlib
STRIP=arm-none-eabi-strip
OBJC=arm-none-eabi-objcopy
OBJD=arm-none-eabi-objdump

#-------------------------------------------------------------
# Configuration
#-------------------------------------------------------------
MCU=arm7tdmi
CDEBUGFLAGS=-g3 -gdwarf-2
CFLAGS=-I$(HEAD_DIR) -O0 -Wall -c -fmessage-length=0 -mapcs-frame -MMD -MP -mcpu=$(MCU)
#CFLAGS_DEP= -Wa,-adhlns="lab4.o.lst" -MF"lab4.d" -MT"lab4.d" -o "lab4.o" "../lab4.c"

LDLIBS=$(LIBS:%=-l%)
LDFLAGS=$(LDLIBS) -nostartfiles -mcpu=$(MCU)
#LDFLAGS_DEP=-T"../lab4.ld" -Wl,-Map,lab4.map -o "lab4.elf" ./lab4.o


#-------------------------------------------------------------
# Rules
#-------------------------------------------------------------
.PHONY: $(TARGET)_release

.PHONY: $(TARGET)_debug

.PHONY: clean



$(TARGET)_release: $(RELEASE_FLASH) $(RELEASE_EXEC)
	@echo '================= FINISHED building: $@ =========================='
	@echo ''

$(TARGET)_debug: $(DEBUG_FLASH) $(DEBUG_EXEC)
	@echo '================= FINISHED building: $@ =========================='
	@echo ''

$(RELEASE_FLASH): $(RELEASE_EXEC)
	@echo -e '\tFLASH $@'
	@$(OBJC) -O binary $< "$@"


$(DEBUG_FLASH): $(DEBUG_EXEC)
	@echo -e '\tFLASH $@'
	@$(OBJC) -O binary $< "$@"


$(RELEASE_EXEC): $(RELEASE_OBJS)
	@echo -e '\tLD $@'
	@$(CC) -ffreestanding -T"lab4.ld" -nostartfiles -L$(LIB_RELEASE_DIR) -Wl,-Map,Release_/lab4.map -mcpu=arm7tdmi -o "Release_/lab4.elf" $(RELEASE_OBJS) $(LIBS)


$(DEBUG_EXEC): $(DEBUG_OBJS)
	@echo -e '\tLD $@'
	@$(CC) -ffreestanding -T"lab4.ld" -nostartfiles -L$(LIB_DEBUG_DIR) -Wl,-Map,Debug_/lab4.map -mcpu=arm7tdmi -g3 -gdwarf-2 -o "Debug_/lab4.elf" $(DEBUG_OBJS) $(LIBS)


$(RELEASE_OBJ_DIR)/%.o: %.c
	@echo -e '\tCC $<'
	@$(CC) -ffreestanding -I../BSP/include -O0 -Wall -Wa,-adhlns="Release_/lab4.lst" -c -fmessage-length=0 -mapcs-frame -MMD -MP -MF"Release_/lab4.d" -MT"Release_/lab4.d" -mcpu=arm7tdmi -o "$@" "$<"


$(DEBUG_OBJ_DIR)/%.o: %.c
	@echo -e '\tCC $<'
	@$(CC) -ffreestanding -I../BSP/include -O0 -Wall -Wa,-adhlns="Debug_/lab4.lst" -c -fmessage-length=0 -mapcs-frame -MMD -MP -MF"Debug_/lab4.d" -MT"Debug_/lab4.d" -mcpu=arm7tdmi -g3 -gdwarf-2 -o "$@" "$<"



$(DEBUG_OBJS): | $(DEBUG_DIR)


$(RELEASE_OBJS): | $(RELEASE_DIR)


$(RELEASE_DIR):
	@mkdir $(RELEASE_DIR)
	@mkdir $(RELEASE_OBJ_DIR)

$(DEBUG_DIR):
	@mkdir $(DEBUG_DIR)
	@mkdir $(DEBUG_OBJ_DIR)


clean:
	rm -rf $(RELEASE_DIR)
	rm -rf $(DEBUG_DIR)
