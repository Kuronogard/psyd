#=============================================================================================$
#  Daniel Pinto Rivero  - Madrid, 2014
#  Universidad Complutense de Madrid
#
#  PSyD - Ing.Computadores
#=============================================================================================$

#-------------------------------------------------------------
# Projects
#-------------------------------------------------------------
LIB=BSP

LABS=lab1 lab2 lab3 lab4 lab5 lab6 lab7 lab8 lab9-wav lab9-efectos

RELEASE_LIB=$(LIB:%=%_release)
DEBUG_LIB=$(LIB:%=%_debug)

RELEASE_LABS=$(LABS:%=%_release)
DEBUG_LABS=$(LABS:%=%_debug)

CLEAN_LIB=$(LIB:%=%_clean)
CLEAN_LABS=$(LABS:%=%_clean)



#-------------------------------------------------------------
# Directories
#-------------------------------------------------------------


#-------------------------------------------------------------
# Programs
#-------------------------------------------------------------


#-------------------------------------------------------------
# Configuration
#-------------------------------------------------------------


#-------------------------------------------------------------
# Rules
#-------------------------------------------------------------

help:
	@echo -e "========= RULES ========================================================================"
	@echo -e "- help			-> Prints this help"
	@echo -e "- release_all		-> Compile everything"
	@echo -e "- debug_all		-> Compile everything with debug options"
	@echo -e "- clean_all		-> Clean everything"
	@echo -e "- %_release		-> Compile % project"
	@echo -e "- %_debug		-> Compile % project with debug òptions"
	@echo -e "- %_clean		-> Clean % project"
	@echo -e "- start_putty	-> Starts a putty serial terminal in ttyUSB0"
	@echo -e "- start_openocd	-> Starts an openOCD connection in ttyUSB0"
	@echo -e ""
	@echo -e "To start a debbugging session, first, must start openOCD invocating" 
	@echo -e "  $$ make start_openocd".
	@echo -e "Then, in another terminal, start gdb and connect to openocd with:"
	@echo -e "  $$ arm-none-eabi-gdb \"exec_file\".elf"
	@echo -e "  $$ (gdb) target remote :3333"
	@echo -e "  $$ (gdb) monitor reset halt"
	@echo -e "  $$ (gdb) load"
	@echo -e "  $$ (gdb) continue"
	@echo -e ""
	@echo -e ""
	@echo -e "An alternative way is to use a pipe to connect gdb and openOCD via stdin/stdout:"
	@echo -e "  $$ make start_openocd"
	@echo -e "  $$ (gdb) target remote | openocd -c \"gdb_port pipe; log_output openocd.log\""
	@echo -e ""

release_all: $(RELEASE_LIB) $(RELEASE_LABS) 

debug_all: $(DEBUG_LIB) $(DEBUG_LABS)

clean_all: $(CLEAN_LIB) $(CLEAN_LABS)

start_putty:
	@putty -load PSyD_serial_connection

start_openocd:
	@sudo openocd -f test/arm-fdi-ucm.cfg

start_gdb:
	arm-none-eabi-gdb 

%_release:
	@$(MAKE) -C $(patsubst %_release,%,$@) $@

%_debug:
	@$(MAKE) -C $(patsubst %_debug,%,$@) $@

%_clean:
	@$(MAKE) -C $(patsubst %_clean,%,$@) clean
